import nltk
from nltk.classify import NaiveBayesClassifier
import collections
from nltk.classify.util import accuracy
from nltk.metrics import BigramAssocMeasures
from nltk.probability import FreqDist, ConditionalFreqDist
from sklearn.svm import LinearSVC
import nltk.classify

class HybridTrain:

	def word_dict(self, words):
		return dict([(w,True) for w in words])
		
	def get_features(self,sents,labels, feature_detector):
		feature_set = collections.defaultdict(list)
		k = 0
		for sent in sents:
			w = nltk.tokenize.word_tokenize(sent)
			feature_set[labels[k]].append(feature_detector(w))
			k = k + 1
		return feature_set

	def split_train_test(self,lf,split=0.8):
		train=[]
		test=[]
		for label,feats in lf.items():
			cutoff=int(len(feats)*split)
			train.extend([(feat,label) for feat in feats[:cutoff]])
			test.extend([(feat,label) for feat in feats[cutoff:]])
		return train,test
	
	def high_information_words(self, labeled_words, score_fn=BigramAssocMeasures.chi_sq, min_score=5):
		word_fd = FreqDist()
		label_word_fd = ConditionalFreqDist()
		
		for label, words in labeled_words:
			for word in words:
				word_fd[word] += 1
				label_word_fd[label][word] += 1
			
		n_xx = label_word_fd.N()
		high_info_words = set()
		
		for label in label_word_fd.conditions():
			n_xi = label_word_fd[label].N()
			word_scores = collections.defaultdict(int)
			
			for word, n_ii in label_word_fd[label].items():
				n_ix = word_fd[word]
				score = score_fn(n_ii, (n_ix, n_xi), n_xx)
				word_scores[word] = score
				
			bestwords = [word for word, score in word_scores.items() if score >= min_score]
			high_info_words |= set(bestwords)
		return high_info_words


	def bag_of_words_in_set(self, words, goodwords):
		return self.word_dict(set(words) & set(goodwords))

	def svm_train(self,sents,labels):
		
		labelwords=[]
		k=0
		for sent in sents:
			labelwords.append((labels[k],nltk.tokenize.word_tokenize(sent)))
			k=k+1
		high_info_words=set(self.high_information_words(labelwords))
		feat_det=lambda words:self.bag_of_words_in_set(words,high_info_words)
		features = self.get_features(sents,labels,feature_detector=feat_det)
		train_data, test_data = self.split_train_test(features)
		svm_class = nltk.classify.SklearnClassifier(LinearSVC()).train(train_data)
		print('Accuracy = '+ str(accuracy(svm_class, test_data)*100)+'%')
		return svm_class
        
	def nbc_train(self,sents,labels):
		labelwords=[]
		k=0
		for sent in sents:
			labelwords.append((labels[k],nltk.tokenize.word_tokenize(sent)))
			k=k+1
		high_info_words = set(self.high_information_words(labelwords))
		feat_det = lambda words:self.bag_of_words_in_set(words,high_info_words)
		features = self.get_features(sents,labels,feature_detector=feat_det)
		train_data, test_data = self.split_train_test(features)
		nbc_class = NaiveBayesClassifier.train(train_data)
		print('Accuracy = '+ str(accuracy(nbc_class, test_data)*100)+'%')
		return nbc_class