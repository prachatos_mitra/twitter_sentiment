import nltk
import sys
from nltk.classify import NaiveBayesClassifier
from preprocess import PreProcessor
from hybridtrain import HybridTrain
import json
class TwitterSentiment:
	def __init__(self):
		self.FILE = 'data/twitterData.csv'
	
	def run(self):
		file=open(self.FILE,encoding="utf8").readlines()
		try:
			count = sys.argv[1]
		except Exception:
			count = 100
		sents, labels = self.del_preprocess(file, count)
		try:
			type = sys.argv[2]
		except Exception:
			type = "nbc"
		if len(type) > 0:
			if type.lower() == "svm":
				type = "svm"
			else:
				type = "nbc"
		else:
			type = "nbc"
		print("Training using " + type.upper())
		classifier = self.del_classify(sents, labels, type)
		classify_text = input('Enter a tweet to assess sentiment: ')
		classify_text = PreProcessor.preprocess([classify_text]) #ideally here we should calculate lines
		for lines in classify_text:
			print(classifier.classify(HybridTrain().word_dict(nltk.tokenize.word_tokenize(lines))))
		input("Press enter to end")
		
	def del_preprocess(self, file, count):
		sents = []
		labels = []
		k = 1
		for line in file:
			split_line = line.split(",")
			if len(split_line) == 3 and k < int(count):
				sents.append(split_line[2])
				labels.append(split_line[0])
				k = k + 1		
		sents_pre = PreProcessor.preprocess(sents)
		return sents_pre, labels

	def del_classify(self, sents, labels, type):
		if type == "svm":
			return HybridTrain().svm_train(sents, labels)
		else:
			return HybridTrain().nbc_train(sents, labels)
		
sent = TwitterSentiment()
sent.run()
