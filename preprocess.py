import nltk
from nltk.classify import NaiveBayesClassifier
import re
from nltk.corpus import wordnet
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

class PreProcessor:
	@staticmethod
	def preprocess(sents):
		sents = PreProcessor.remove_apo(sents)
		sents = PreProcessor.remove_hash(sents)
		sents = PreProcessor.remove_at(sents)
		sents = PreProcessor.remove_links(sents)
		sents = PreProcessor.remove_stopwords(sents)
		sents = PreProcessor.remove_spaces_specials(sents)
		sents = PreProcessor.remove_extra_chars(sents)
		return sents
	
	@staticmethod
	def remove_at(sents):
		parsed_sent = []
		for sent in sents:
			sent = re.sub('@[A-Za-z0-9]+', '', sent) #find @word
			parsed_sent.append(re.sub(r'(\s)+',' ',sent))
		return parsed_sent
		
	@staticmethod
	def remove_hash(sents):
		parsed_sent = []
		for sent in sents:
			hash_tags=re.findall('#[A-Za-z0-9]+', sent) #find hashtags
			for word in hash_tags:
				new_word=re.sub('#','',word)
				if wordnet.synsets(new_word): #if in synsets, keep, else discard
					sent=re.sub('#'+new_word,new_word, sent) 
				else:
					sent=re.sub('#'+new_word,'', sent)
			parsed_sent.append(re.sub(r'(\s)+',' ', sent))  
		return parsed_sent
		
	@staticmethod
	def remove_links(sents):
		parsed_sent = []
		for sent in sents:
			sent = re.sub('(\w+:\/\/\S+)','',sent)
			parsed_sent.append(re.sub(r'(\s)+',' ',sent))
		return parsed_sent
		
	@staticmethod
	def remove_stopwords(sents):
		st=stopwords.words('english')
		parsed_sent=[]
		for sent in sents:
			word_sent=word_tokenize(sent)
			new_word=[w for w in word_sent if w not in st]
			parsed_sent.append(' '.join(new_word))   
		return parsed_sent
		
	@staticmethod
	def replace(word):
		w=word
		if wordnet.synsets(w): #if in synsets
			return w
		new_word=re.sub(r'(\w*)(\w)\2(\w*)',r'\1\2\3',w) 
		if new_word != w:
			return PreProcessor.replace(new_word)
		else:
			return w
	
	@staticmethod
	def remove_extra_chars(sents):
		parsed_sent=[]
		for sent in sents:
			sent=re.sub('\.','',sent)
			for w in word_tokenize(sent):
				sent=re.sub(w,PreProcessor.replace(w),sent)
			parsed_sent.append(sent)
		return parsed_sent

	@staticmethod
	def remove_apo(sents):
		a=[(r'won\'t', 'will not'),(r'can\'t', 'cannot'),(r'\'m', 'i am'),(r'ain\'t', 'is not'),(r'(\w+)\'ll', '\g<1> will'),(r'(\w+)n\'t', '\g<1> not'),(r'(\w+)\'ve', '\g<1> have'),(r'(\w+)\'s', '\g<1> is'),(r'(\w+)\'re', '\g<1> are'),(r'(\w+)\'d', '\g<1> would')]
		parsed_sent=[]
		for sent in sents:
			for (n,m) in a:
				sent=re.sub(re.compile(n),m,sent)
			parsed_sent.append(sent)
		return parsed_sent

	@staticmethod
	def remove_spaces_specials(sents):
		parsed_sent=[]
		for sent in sents:
			sent=re.sub(r'[^A-Za-z0-9]',' ',sent)
			sent=re.sub(r'(\s)+',' ',sent)
			parsed_sent.append(sent)
		return parsed_sent
		
	
	
		